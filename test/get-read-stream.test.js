const { describe } = require('tape-plus')
const ArtefactStore = require('..')
const fs = require('fs')
const path = require('path')
const ram = require('random-access-memory')
const { promisify } = require('util')
const readFile = promisify(fs.readFile)

describe('artefactStore.getReadStream', (context) => {
  context('retrieve one own artefact', async (assert, next) => {
    const originalFilePath = path.join(path.resolve(__dirname), './data/README.md')
    const storeFilePath = './read-me.md'

    const key = ArtefactStore.artefactEncryptionKey()
    const alice = new ArtefactStore(ram)
    await alice.ready()
    const source = fs.createReadStream(originalFilePath)
    const target = alice.createWriteStream(storeFilePath, key)
    source.pipe(target)
    await alice.fileReady(storeFilePath)

    const sourceData = await readFile(originalFilePath, 'utf8').catch((err) => { throw err })
    assert.ok(sourceData, 'file exists on filesystem')
    let targetData = ''

    const stream = await alice.getReadStream(storeFilePath, alice.driveAddress, key)
    for await (const data of stream) { targetData += data.toString('utf8') }

    assert.equal(sourceData, targetData, 'source copied to target')

    alice.close()
    next()
  })

  context('retrieve latter part of one own artefact', async (assert, next) => {
    const key = ArtefactStore.artefactEncryptionKey()
    const alice = new ArtefactStore(ram)
    await alice.ready()
    const source = fs.createReadStream(path.join(path.resolve(__dirname), './data/README.md'))
    const target = alice.createWriteStream('./README.md', key)
    source.pipe(target)
    await alice.fileReady('./README.md')
    let sourceData = ''
    const streamFS = fs.createReadStream(path.join(path.resolve(__dirname), './data/README.md'), { start: 100 })
    for await (const data of streamFS) { sourceData += data.toString('utf8') }
    let targetData = ''
    const aliceStream = await alice.getReadStream('./README.md', alice.driveAddress, key, { start: 100 })
    for await (const data of aliceStream) { targetData += data.toString('utf8') }
    assert.equals(sourceData, targetData, 'source copied to target')
    alice.close()
    next()
  })
})
