const { describe } = require('tape-plus')
const ArtefactStore = require('..')
const fs = require('fs')
const path = require('path')
const { promisify } = require('util')
const localUtil = require('./helpers/util')
const stat = promisify(fs.stat)

describe('artefactStore.remove', (context) => {
  context('remove one artefact from a store', async (assert, next) => {
    const originalFilePath = path.join(path.resolve(__dirname), './data/README.md')
    const storeFilePath = './READ.md'

    // establish original file size
    const readmeStats = await stat(originalFilePath)
    const readmeSize = readmeStats.size

    const testStorage = localUtil.makeTmpDir()
    const key = ArtefactStore.artefactEncryptionKey()
    const alice = new ArtefactStore(testStorage)
    await alice.ready()

    // add file into store
    const source = fs.createReadStream(originalFilePath)
    const target = alice.createWriteStream(storeFilePath, key)
    source.pipe(target)
    await alice.fileReady(storeFilePath)

    // make sure files are the same
    let originalData = ''
    const streamFS = fs.createReadStream(originalFilePath)
    for await (const data of streamFS) { originalData += data }

    let storeData = ''
    const aliceStream = await alice.getReadStream(storeFilePath, alice.driveAddress, key)
    for await (const data of aliceStream) { storeData += data.toString('utf8') }

    assert.equals(originalData.toString('utf8'), storeData, 'files should be same')

    // establish size of drive before file removed
    const sizeBefore = await localUtil.totalSize(testStorage)
    // remove file
    await alice.remove(storeFilePath, alice.driveAddress)
    // establish size of drive after file removed
    const sizeAfter = await localUtil.totalSize(testStorage)
    // establish difference between size of drive before and after file removed
    const sizeDiff = sizeBefore - sizeAfter
    // check sizeDiff equals fileSize
    assert.equals(sizeDiff, readmeSize, 'file successfully removed')
    alice.close()
    await localUtil.rmTmpDir(testStorage)
    next()
  })
})
