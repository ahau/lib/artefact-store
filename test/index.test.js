const { describe } = require('tape-plus')
const ArtefactStore = require('..')
const fs = require('fs')
const path = require('path')
const ram = require('random-access-memory')
const { promisify } = require('util')
const readFile = promisify(fs.readFile)
const localUtil = require('./helpers/util')

describe('artefactStore.(misc)', (context) => {
  context('replicate one user corestore to another using networker', async (assert, next) => {
    const key = ArtefactStore.artefactEncryptionKey()
    const alice = new ArtefactStore(ram)
    const bob = new ArtefactStore(ram)
    await Promise.all([
      alice.ready(),
      bob.ready()
    ])

    const source = fs.createReadStream(path.join(path.resolve(__dirname), './data/README.md'))
    const target = alice.createWriteStream('./README.md', key)
    source.pipe(target)
    await alice.fileReady('./README.md')
    const streamAliceReadme = await bob.getReadStream('./README.md', alice.driveAddress, key)

    let aliceData = ''
    for await (const data of streamAliceReadme) { aliceData += data }
    const sourceData = await readFile(path.join(path.resolve(__dirname), './data/README.md'), 'utf8').catch((err) => { throw err })
    assert.ok(sourceData, 'aliceData on filesystem')
    assert.equal(aliceData, sourceData, 'bob has replicated alice\'s hyperdrive')

    alice.close()
    bob.close()
    next()
  })

  context('on restart, reconnect to previously loaded remote drives', async (assert, next) => {
    const testStorageAlice = localUtil.makeTmpDir()
    const testStorageBob = localUtil.makeTmpDir()
    const key = ArtefactStore.artefactEncryptionKey()

    const alice = new ArtefactStore(testStorageAlice)
    const bob = new ArtefactStore(testStorageBob)
    await Promise.all([
      alice.ready(),
      bob.ready()
    ])

    // Bob writes files to his drive
    const filePath1 = path.resolve(__dirname, './data/README.md')
    await bob.addFile(filePath1, { optionalFileEncryptionKey: key })

    const fileSource2 = fs.createReadStream(path.resolve(__dirname, './data/TESTFILE.md'))
    await bob.addFileStream(fileSource2, './TESTFILE.md', { optionalFileEncryptionKey: key })
    // NOTE changed to use addFile, addFileStream to give those methods some minimal coverage

    // Alice can get the README file from bob
    const aliceGetFile1 = await alice.getReadStream('./README.md', bob.driveAddress, key)
    let aliceFile1 = ''
    for await (const data of aliceGetFile1) { aliceFile1 += data }
    const sourceFile1 = await readFile(path.resolve(__dirname, './data/README.md'), 'utf8').catch((err) => { throw err })
    assert.equal(aliceFile1, sourceFile1, 'alice is connected to bob and has replicated file1 from bob\'s hyperdrive')
    // Alice restarts
    await alice.close()

    const aliceRestart = new ArtefactStore(testStorageAlice)
    await aliceRestart.ready()
    assert.ok(aliceRestart.drives.remote.has(bob.driveAddress.toString('hex')), 'after restart, alice automatically reconnects to bob')

    const aliceGetFile2 = await aliceRestart.getReadStream('./TESTFILE.md', bob.driveAddress, key)
    let aliceFile2 = ''
    for await (const data of aliceGetFile2) { aliceFile2 += data }
    const sourceFile2 = await readFile(path.resolve(__dirname, './data/TESTFILE.md'), 'utf8').catch((err) => { throw err })
    assert.equal(aliceFile2, sourceFile2, 'alice is connected to bob and has replicated file2 from bob\'s hyperdrive')

    aliceRestart.close()
    bob.close()
    await localUtil.rmTmpDir(testStorageAlice)
    await localUtil.rmTmpDir(testStorageBob)
    next()
  })

  context('sparse replication is sparse!', async (assert, next) => {
    const testStorageAlice = localUtil.makeTmpDir()
    const testStorageBob = localUtil.makeTmpDir()
    const key = ArtefactStore.artefactEncryptionKey()

    const alice = new ArtefactStore(testStorageAlice)
    const bob = new ArtefactStore(testStorageBob)
    await Promise.all([
      alice.ready(),
      bob.ready()
    ])

    // Bob writes files to his drive
    const file1 = fs.createReadStream(path.resolve(__dirname, './data/README.md'))
    const bobFile1 = bob.createWriteStream('./README.md', key)
    file1.pipe(bobFile1)
    await bob.fileReady('./README.md')

    await new Promise(resolve => setTimeout(resolve, 1000)) // wait to see if replication stays sparse

    const size1 = await alice.driveSize(bob.driveAddress) // alice's local, sparse copy of bob's drive
    assert.equal(size1, 0, 'alice has not replicated anything of bobs')

    // Alice restarts
    await alice.close()

    const aliceRestart = new ArtefactStore(testStorageAlice)
    await aliceRestart.ready()

    await new Promise(resolve => setTimeout(resolve, 1000)) // wait to see if replication stays sparse

    const size2 = await aliceRestart.driveSize(bob.driveAddress) // alice's local, sparse copy of bob's drive
    assert.equal(size1, size2, 'drive does not magically replicate more after connect')

    const aliceGetFile2 = await aliceRestart.getReadStream('./README.md', bob.driveAddress, key)
    let aliceFile2 = ''
    for await (const data of aliceGetFile2) { aliceFile2 += data } // eslint-disable-line
    // just pulling the file over

    const size3 = await aliceRestart.driveSize(bob.driveAddress) // alice's local, sparse copy of bob's drive

    assert.true(size3 > size1, 'after requesting a file, driveSize increases')
    // console.log(size1, size2, size3)

    aliceRestart.close()
    bob.close()
    await localUtil.rmTmpDir(testStorageAlice)
    await localUtil.rmTmpDir(testStorageBob)
    next()
  })

  context('add and retrieve artefacts with unusual fileNames', async (assert, next) => {
    // nb. the path appears not to work if a `\` character is used in the name
    const originalFilePath = path.join(path.resolve(__dirname), './data/%&!"£$%^&*()@+=.ed25519')
    const storeFilePath = '%&!"£$%^&*()@+=.ed25519'

    const key = ArtefactStore.artefactEncryptionKey()
    const alice = new ArtefactStore(ram)
    await alice.ready()
    const source = fs.createReadStream(originalFilePath)
    const target = alice.createWriteStream(storeFilePath, key)
    source.pipe(target)
    await alice.fileReady(storeFilePath)

    const sourceData = await readFile(originalFilePath, 'utf8').catch((err) => { throw err })
    assert.ok(sourceData, 'file exists on filesystem')
    let targetData = ''

    const stream = await alice.getReadStream(storeFilePath, alice.driveAddress, key)
    for await (const data of stream) { targetData += data.toString('utf8') }

    assert.equal(sourceData, targetData, 'source copied to target')

    alice.close()
    next()
  })
})
