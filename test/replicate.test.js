const { describe } = require('tape-plus')
const ram = require('random-access-memory')

const ArtefactStore = require('..')

describe('artefactStore._replicate', (context) => {
  context('test two stores can be replicated', async (assert, next) => {
    const alice = new ArtefactStore(ram)
    const bob = new ArtefactStore(ram)
    await Promise.all([alice.ready(), bob.ready()])

    function replicate (cb) {
      const stream = alice._replicate(true, { live: true })
      stream.pipe(bob._replicate(false, { live: true })).pipe(stream) // This will replicate all common cores.
      return process.nextTick(cb, null)
    }

    replicate((err, _) => {
      assert.error(err, 'replicates without error')

      alice.close()
      bob.close()
      next()
    })
  })
})
