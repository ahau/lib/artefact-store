# artefact-store 

This is a small module that will plug into the back-end of Ahau for the storage and retrieval of 'artefacts': text, image, audio or video files.

## Example Usage

TODO

## API

### `new ArtefactStore(storagePath, options) => artefactStore`

Create a new `artefactStore` instance.
- `storagePath` can be either a string or a random-access-storage module.
- `options` is an optional object which may include:
    - `bootstrap: [addresses]` - override the default DHT bootstrap nodes and use your own ones.  Taken an array of strings of the form `host:port`.

You can run your own DHT nodes using `https://github.com/hyperswarm/cli`, and then pass the host and port in here.

If you want to include the default nodes as well, they are:
`[
  'bootstrap1.hyperdht.org:49737',
  'bootstrap2.hyperdht.org:49737',
  'bootstrap3.hyperdht.org:49737'
]`

### `ArtefactStore.artefactEncryptionKey() => Buffer`

Static function which generates a unique key for encrypting each message that is added to a user's own artefactStore. Returns a buffer.



### `artefactStore.ready() => Promise`

Call after instantiating ArtefactStore, ensures user's own drive keys are correctly recorded. Returns a promise which resolves when artefactStore is fully initialised.

### `artefactStore.driveAddress => String`

Returns the driveAddress (as a String) for this instance.

### `artefactStore.createWriteStream(artefactFileName, artefactEncryptionKey, opts)`

Prepare to write a file to one of a user's own drives within their artefactStore. 
- `artefactFileName` should be a string.
- `artefactEncryptionKey` is a unique key required to encrypt the message being written. Should either be a string encoded in hex or a buffer.
- `opts` is an optional object that could contain the same options offered by fs.createWriteStream.

### `artefactStore.getReadStream(artefactFileName, driveAddress, artefactEncryptionKey, opts = {}) => Promise`

Prepare to read a file from a particular drive within an artefactStore. Unlike createReadStream, this returns a promise which, when resolved, will give a stream.
- `artefactFileName` should be a string.
- `driveAddress` should either be a string encoded in hex or a buffer.
- `artefactEncryptionKey` is a unique key required to decrypt the message being read. Should either be a string encoded in hex or a buffer.
- `opts` is an optional object. As with fs.createReadStream, it can contain `start: number` to indicate the position in a given file at which to start the readstream, and `end: number` to indicate the position in a given file at which to stop the readstream.

### `artefactStore.update(fileName) => Promise`

When writing to a hyperdrive, this method ensures the hyperdrive has updated and the file `fileName` is accessible once written.


### `artefactStore._replicate(isInitiator, opts)`

Internal method. Replicates one user's artefactStore to another's. `isInitiator` should be `true` for the artefactStore initiating the exchange, and `false` for the artefactStore responding. Returns a stream. Note: this is currently not used in this module, as corestore-networker does this internally, but is available in case it is desirable to have another method of replicating from peers.

`await artefactStore.remove(artefactFileName, driveAddress)`

Given an artefactFileName and its driveAddress, deletes the local copy of the file. Used to clear space on local device. NB this method does not check that there is another copy of the file existing on the network - ie. that this is not the only existing copy.

### `artefactStore.close() => Promise`

Gracefully closes connections to peers.

### artefactStore.fileSize(artefactFileName, driveAddress) => Promise`

Given an artefactFileName and its driveAddress, returns the size in bytes of the file. Useful for telling the browser the size of a file so that it can effectively skip within the file while streaming.

### `artefactStore.driveSize(driveAddress) => Promise`

Given a driveAddress, returns the size in bytes of the locally stored copy of the drive.

