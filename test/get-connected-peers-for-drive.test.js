const { describe } = require('tape-plus')
const ram = require('random-access-memory')

const ArtefactStore = require('..')

describe('artefactStore.getConnectedPeersForDrive', (context) => {
  context('Check number of connected peers', async (assert, next) => {
    const alice = new ArtefactStore(ram)
    const bob = new ArtefactStore(ram)
    await Promise.all([
      alice.ready(),
      bob.ready()
    ])

    await alice.addDrive(bob.driveAddress)
    const peers = await bob.getConnectedPeersForDrive()
    assert.equal(peers.length, 1, 'Correct number of connected peers')
    alice.close()
    bob.close()
    next()
  })
})
