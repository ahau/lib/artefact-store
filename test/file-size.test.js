const { describe } = require('tape-plus')
const fs = require('fs')
const path = require('path')
const { promisify } = require('util')

const ArtefactStore = require('..')
const localUtil = require('./helpers/util')
const stat = promisify(fs.stat)

describe('artefactStore.fileSize', (context) => {
  context('find the size of a given file on a drive', async (assert, next) => {
    const testStorageAlice = localUtil.makeTmpDir()
    const testStorageBob = localUtil.makeTmpDir()
    const key = ArtefactStore.artefactEncryptionKey()

    const alice = new ArtefactStore(testStorageAlice)
    const bob = new ArtefactStore(testStorageBob)
    await Promise.all([
      alice.ready(),
      bob.ready()
    ])

    const file1 = fs.createReadStream(path.join(__dirname, './data/README.md'))
    const bobFile1 = bob.createWriteStream('./README.md', key)
    file1.pipe(bobFile1)
    await bob.fileReady('./README.md')

    const originalFileStats = await stat(path.join(__dirname, './data/README.md'))
    const originalFileSize = originalFileStats.size

    const driveFileSize = await alice.fileSize('README.md', bob.driveAddress)
    assert.equals(originalFileSize, driveFileSize, 'alice successfully determines the size of a file on bob\'s drive')

    alice.close()
    bob.close()

    await Promise.all([
      localUtil.rmTmpDir(testStorageAlice),
      localUtil.rmTmpDir(testStorageBob)
    ])
    next()
  })
})
