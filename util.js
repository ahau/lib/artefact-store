const sodium = require('sodium-native')
const KEY_ENCODING = 'hex'

function toString (bufOrString = '') {
  return typeof bufOrString === 'string'
    ? bufOrString
    : bufOrString.toString(KEY_ENCODING)
}

function toBuffer (bufOrString = '') {
  return Buffer.isBuffer(bufOrString)
    ? bufOrString
    : Buffer.from(bufOrString, KEY_ENCODING)
}

module.exports = {
  edToCurvePk (publicKey) {
    const curvePublicKey = sodium.sodium_malloc(sodium.crypto_box_PUBLICKEYBYTES)
    sodium.crypto_sign_ed25519_pk_to_curve25519(curvePublicKey, toBuffer(publicKey))
    return curvePublicKey
  },
  edToCurveSk (secretKey) {
    const curveSecretKey = sodium.sodium_malloc(sodium.crypto_box_SECRETKEYBYTES)
    sodium.crypto_sign_ed25519_sk_to_curve25519(curveSecretKey, toBuffer(secretKey))
    return curveSecretKey
  },
  printKey (buf) {
    return toString(buf).slice(0, 4)
  },
  toString,
  toBuffer,
  isKey (thing, lengthInBytes) {
    return toBuffer(thing).length === lengthInBytes
  }
}
